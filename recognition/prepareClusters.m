function [outputClusters] = prepareClusters(inputClusters)
%PREPARECLUSTERS Prepare clusters for recognition
%   Counts means for all clusters
for i = 1:length(inputClusters)
    samples = inputClusters(i).samples;
    samplesCount = length(samples);
    sizes = zeros(samplesCount, 2);
    for j = 1:samplesCount
       sizes(j, :) = samples(j).size;
    end
    maxSize = max(sizes, [], 1);
    sumSample = zeros(maxSize);
    sumDist = 0;
    for j = 1:samplesCount
        sumSample(1:sizes(j, 1), 1:sizes(j, 2)) =...
            sumSample(1:sizes(j, 1), 1:sizes(j, 2)) + samples(j).img;
        sumDist = sumDist + samples(j).staveDist;
    end
    inputClusters(i).meanSample = sumSample / samplesCount;
    inputClusters(i).meanDist = sumDist / samplesCount;
end
outputClusters = inputClusters;
end

