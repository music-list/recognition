function [class, classesMaxEstimation] = calculateClass(img, staveDist, clustersList, varargin)
%CALCULATETYPE Summary of this function goes here
%   Detailed explanation goes here
p = inputParser;
addRequired(p, 'img');
addRequired(p, 'staveDist');
addRequired(p, 'clustersList');
addOptional(p, 'Types', []);
parse(p, img, staveDist, clustersList, varargin{:});
types = p.Results.Types;
clustersCount = length(clustersList);
classesEstimation = zeros(clustersCount, 2);
maximalSizeDifference = 2;
% Normalize
imgSize = size(img);
img = img - mean(img(:));
normalisedSize = imgSize / staveDist;
s = max(imgSize);
pad = floor(([s s] - imgSize)/2);
img = padarray(img, pad);
for i = 1:clustersCount
    classesEstimation(i, 1) = clustersList(i).mark;
    if ~isempty(types)
        % Check, should this cluster type be recognised
        if ~types(clustersList(i).mark).useInRecognition
            classesEstimation(i, 2) = -255;
            continue;
        end
    end
    clusterMean = clustersList(i).meanSample;
    clusterMean = clusterMean - mean(clusterMean(:));
    %Filter normalised sample size
    meanNormalisedSize = size(clusterMean) / clustersList(i).meanDist;
    sizeK = meanNormalisedSize - normalisedSize;
    if abs(sizeK(1)) > maximalSizeDifference || abs(sizeK(2)) > maximalSizeDifference
        % Have other size
        classesEstimation(i, 2) = -254;
        continue;
    end
    % If size is ok:
    % Pad to square
    meanSize = size(clusterMean);
    s = max(meanSize);
    pad = floor(([s s] - meanSize)/2);
    clusterMean = padarray(clusterMean, pad);
    % Resize sample
    clusterMean = imresize(clusterMean, size(img));
    % Count distance
    corrMatrix = normxcorr2(clusterMean, img);
    classesEstimation(i, 2) = max(corrMatrix(:));
end
classes = unique(classesEstimation(:, 1));
classesMaxEstimation = zeros(length(classes), 2);
for i=1:length(classes)
    classesMaxEstimation(i, 1) = classes(i);
    classIdx = classesEstimation(:, 1) == classes(i);
    classesMaxEstimation(i, 2) = max(classesEstimation(classIdx, 2));
end
[maxChance, maxIdx] = max(classesMaxEstimation(:, 2));
if maxChance < 0.53
    class = 1; %Not found
else
    class = classesMaxEstimation(maxIdx, 1);    
end
end

