function [centerPoint] = getCenter(img, class, staveDist)
%getCenter Get real center of note paper component 
%   Calculate position on note stave
[h, w] = size(img);
if ~islogical(img)
    img = imbinarize(img);
end
switch class
    case {2}
        %���� ��� �����
        centerPoint = [h-staveDist/2; w/2];
    case {5, 6, 3, 4}
        % ���� � ������
        r = staveDist / 2;
        centerPoint = findCirclesCorr(img, r, 1, 'Threshold', 0);
    case 11
        % ���������� ����
        r = staveDist;
        [centerPoint, ~] = findCirclesHough(img, r, 1, 'Threshold', 0);
    case 10
        % ������� ����
        centerPoint = [0; 0];
    otherwise
        %��� ���������
        centerPoint = [h w]/2;
end
end

