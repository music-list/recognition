close all; clear all;
%
addpath('recognition');
addpath('prepare');
load('types.mat', 'types');
%
dir = 'target/centers/';
componentsFiles = {...
    [dir 'dies.png'];...
    [dir 'note1_2-1.png'];...
    [dir 'note1_4-1.png'];...
    [dir 'treble_clef.png'];...
    [dir 'note1_8-1.png'];...
    [dir 'note1_8-2.png'];...
    [dir 'note1_8-3.png'];...
    [dir 'note1_8_down-1.png'];...
};
componentsInfo = [...
    8, 18;
    3, 18;
    4, 18;
    11, 18;
    5, 18;
    5, 18;
    5, 18;
    5, 18;
];
componentCount = length(componentsFiles);
figure;
for i=1:componentCount
    img = im2double(imread(componentsFiles{i}));
    center = getCenter(img, componentsInfo(i, 1), componentsInfo(i, 2), types);
    subplot(1, componentCount, i);
    imshow(img)
    hold on
    scatter(center(2), center(1), 'b*')
    hold off
end