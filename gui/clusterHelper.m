function varargout = clusterHelper(varargin)
% CLUSTERHELPER MATLAB code for clusterHelper.fig
%      CLUSTERHELPER, by itself, creates a new CLUSTERHELPER or raises the existing
%      singleton*.
%
%      H = CLUSTERHELPER returns the handle to a new CLUSTERHELPER or the handle to
%      the existing singleton*.
%
%      CLUSTERHELPER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CLUSTERHELPER.M with the given input arguments.
%
%      CLUSTERHELPER('Property','Value',...) creates a new CLUSTERHELPER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before clusterHelper_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to clusterHelper_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help clusterHelper

% Last Modified by GUIDE v2.5 09-Jun-2019 16:19:26

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @clusterHelper_OpeningFcn, ...
                   'gui_OutputFcn',  @clusterHelper_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before clusterHelper is made visible.
function clusterHelper_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;

addpath('../recognition');
load '../types.mat';
handles.types = types;
handles.typesLabels = extractfield(types, 'Label');

t = strcat(handles.typesLabels, '\n');
s = sprintf(strcat(t{:}));
set(handles.typesselect, 'String', s);


guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = clusterHelper_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function clusters_CreateFcn(hObject, eventdata, handles)
% hObject    handle to clusters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in samples.
function samples_Callback(hObject, eventdata, handles)
% hObject    handle to samples (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns samples contents as cell array
%        contents{get(hObject,'Value')} returns selected item from samples


% --- Executes during object creation, after setting all properties.
function samples_CreateFcn(hObject, eventdata, handles)
% hObject    handle to samples (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in removebutton.
function removebutton_Callback(hObject, eventdata, handles)
selectedSample = get(handles.samples,'Value');
selectedCluster = handles.selectedCluster;
handles.clustersList(selectedCluster).samples(selectedSample) = [];
guidata(hObject, handles);
updateSamples(handles);

% --- Executes on button press in addbutton.
function addbutton_Callback(hObject, eventdata, handles)
newSampleId = get(handles.idtext, 'String');
selectedCluster = handles.selectedCluster;
%Looking for new item
clusterIdx = 0;
sampleIdx = 0;
for i=1:length(handles.clustersList)
    samples = handles.clustersList(i).samples;
    for j=1:length(samples)
        if samples(j).id == str2num(newSampleId)
            clusterIdx = i;
            sampleIdx = j;
            break
        end
    end
    if sampleIdx ~= 0
        break
    end
end
if sampleIdx == 0
    msgbox(['������� � ID ' newSampleId ' �� ������']);
    return
end
%Move sample
sample = handles.clustersList(clusterIdx).samples(sampleIdx);
handles.clustersList(clusterIdx).samples(sampleIdx) = [];
handles.clustersList(selectedCluster).samples(end+1) = sample;
guidata(hObject, handles);
updateSamples(handles);

% --- Executes on button press in result.
function result_Callback(hObject, eventdata, handles)
[filename, path] = uigetfile('*.mdb.mat');
load([path filename], 'clustersList', 'totalClusterCount', 'lastSampleId');
handles.clustersList = clustersList;
handles.totalClusterCount = totalClusterCount;
handles.lastSampleId = lastSampleId;
refreshClusterList(handles);
guidata(handles.result, handles);

% --- Executes on selection change in clusters.
function clusters_Callback(hObject, eventdata, handles)
% hObject    handle to clusters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selectedCluster = get(hObject,'Value');
handles.selectedCluster = selectedCluster;
clusterMark = handles.clustersList(selectedCluster).mark;
set(handles.typesselect, 'Value', clusterMark);
guidata(handles.typesselect, handles);
updateSamples(handles);

function updateSamples(handles)
selectedCluster = handles.selectedCluster;
currentSamples = handles.clustersList(selectedCluster).samples;
if ~isempty(currentSamples)
    samplesIds = extractfield(currentSamples, 'id');
    set(handles.samples, 'String', samplesIds);
    set(handles.samples, 'Value', 1);
    displayClusterImage(currentSamples, handles);
else
    set(handles.samples, 'String', '');
    set(handles.samples, 'Value', []);
    axes(handles.axes1);
    imshow(ones(1)*0.5);
end

function displayClusterImage(samples, handles)
%Generate cluster image
imgCount = length(samples);
if imgCount == 1
    axes(handles.axes1);
    imshow(samples(1).img);
    return
end
% Generate big image with all samples
sizes = zeros(imgCount, 2);
for i=1:imgCount
    sizes(i, :) = samples(i).size;
end
maxSize = max(sizes);
outProRow = ceil(sqrt(imgCount));
out = ones(maxSize*outProRow)*0.5;
for i=1:imgCount
    y = floor((i - 1) / outProRow) * maxSize(1) + 1;
    x = mod(i-1, outProRow) * maxSize(2) + 1;
    out(y:(y+sizes(i, 1)-1), x:(x+sizes(i, 2)-1)) = samples(i).img;
end
axes(handles.axes1);
imshow(out)


% --- Executes on selection change in typesselect.
function typesselect_Callback(hObject, eventdata, handles)
if ~isfield(handles, 'selectedCluster')
    msgbox('�������� �������!');
end
currentCluster = handles.selectedCluster;
selectedMark = get(hObject,'Value');
handles.clustersList(currentCluster).mark = selectedMark;
refreshClusterList(handles);
guidata(handles.typesselect, handles);

% --- Executes on button press in loadclusters.
function loadclusters_Callback(hObject, eventdata, handles)
[filename, path] = uigetfile('*.clust.mat');
load([path filename], 'imgs', 'meanStaveDist');
%Process new clusters
newSamplesMarks = extractfield(imgs, 'cluster');
newClusters = unique(newSamplesMarks);
newClustersCount = length(newClusters);
if isfield(handles, 'totalClusterCount')
    oldClustersCount = handles.totalClusterCount;
else
    oldClustersCount = 0;
end
if isfield(handles, 'lastSampleId')
    lastSampleId = handles.lastSampleId;
else
    lastSampleId = 0;
end
%Add new clusters to list
for i=1:newClustersCount
    clusterItems = find(newSamplesMarks == i);
    newCluster.id = oldClustersCount + i;
    newCluster.mark = 1;
    newCluster.samples = imgs(clusterItems);
    for j=1:length(clusterItems)
        lastSampleId = lastSampleId + 1;
        newCluster.samples(j).staveDist = floor(meanStaveDist);
        newCluster.samples(j).id = lastSampleId;
        rmfield(newCluster.samples(j), 'cluster');
    end
    handles.clustersList(oldClustersCount+i) = newCluster;
end
handles.totalClusterCount = oldClustersCount + newClustersCount;
handles.lastSampleId = lastSampleId;
refreshClusterList(handles);
guidata(handles.loadclusters, handles);
msgbox('�������� ���������!');

function refreshClusterList(handles)
%Generate list content
clusterIds = extractfield(handles.clustersList, 'id');
for i=1:length(clusterIds)
    color = '#00AA00';
    symbol = '&#9745;';
    if handles.clustersList(i).mark == 1
        color = '#FF0000';
        symbol = '&#9746;';
    end
    clusterLabels{i} = ['<HTML><FONT color="' color '">' symbol '</FONT> ' num2str(clusterIds(i)) '</HMTL>'];
end
set(handles.clusters, 'String', clusterLabels);
guidata(handles.clusters, handles);


% --- Executes on button press in savesamples.
function savesamples_Callback(hObject, eventdata, handles)
[filename, path] = uiputfile('*.mdb.mat');
clustersList = handles.clustersList;
totalClusterCount = handles.totalClusterCount;
lastSampleId = handles.lastSampleId;
save([path filename], 'clustersList', 'totalClusterCount', 'lastSampleId');


% --- Executes during object creation, after setting all properties.
function typesselect_CreateFcn(hObject, eventdata, handles)
% hObject    handle to typesselect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~isfield(handles, 'selectedCluster')
    msgbox('�������� �������!');
end
currentClusterIdx = handles.selectedCluster;
clustersList = prepareClusters(handles.clustersList);
currentCluster = clustersList(currentClusterIdx);
clustersList(currentClusterIdx) = []; %Remove current cluster
clustersMarks = extractfield(clustersList, 'mark');
clustersList = clustersList(clustersMarks > 1); %Use only marked classes
if isempty(clustersList)
    msgbox('� �� ��� ����������� ���������!');
    return;
end
possibleMark = calculateClass(currentCluster.meanSample, currentCluster.meanDist, clustersList);
% Set to gui
set(handles.typesselect, 'Value', possibleMark);
handles.clustersList(currentClusterIdx).mark = possibleMark;
refreshClusterList(handles);
guidata(handles.typesselect, handles);
msgbox(['�������������� �����: ' handles.typesLabels(possibleMark)]);
