function [ wostave ] = removeStave( img, stave, line_h )
    img = imbinarize(img, adaptthresh(img));
    stave = imbinarize(stave,adaptthresh(stave));
    [ih, iw, ~] = size(img);

    f1=img&~stave;
    close_h = ceil(line_h*2);
    f1=f1|imdilate(f1,ones(close_h, 1))&stave;
    it = line_h;
    for k=-it:it
        shift_stave = stave;
        if k == 0
            continue;
        end
        if k > 0
            shift_stave = [stave((1+k):ih, :); zeros(k,iw)];
        end
        if k < 0
            shift_stave = [zeros(-k,iw); stave(1:ih+k,:)];
        end
        f2=img&~shift_stave;
        df = imdilate(f2,ones(close_h))&shift_stave;
        f2=f2|df;
        f1=f1&f2;   
    end
    wostave = f1;
end

