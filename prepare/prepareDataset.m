function [dataset, meanStaveDist, staves, debugInfo] = prepareDataset(I, varargin)
%PREPAREDATASET Cut image into parts and generate dataset
%   
%% PARSE INPUT
p = inputParser;
addRequired(p, 'I');
addOptional(p, 'Debug', 0);
parse(p, I, varargin{:});
debug = p.Results.Debug;
if debug ~= 0
    debugInfo = {};
end
%% Prepare image
[h, w, c] = size(I);
if c == 3
    I = rgb2gray(I);
end
I = double(I);
I = mat2gray(-I);
%% Split into staves
% Get borders of staves
staveCount = 0;
borders = getBorders(I);
if debug ~= 0
    figure; imshow(I);
    hold on;
    plot([0 w], [borders(:, 1) borders(:, 1)], 'g');
    plot([0 w], [borders(:, 2) borders(:, 2)], 'r');
    hold off;
end
% Prepare image for extraction
% Extract and filter staves with notes
pI = I;
for i = 1:length(borders)
   staveHeight = borders(i, 2) - borders(i, 1);
   if staveHeight < h / (length(borders) * 2)
       continue
   end
   staveImg = I(borders(i,1):borders(i, 2), :);
   staveDist = getStaveDist(staveImg, 5);
   if staveDist == 0
       continue
   end
   % Extract stave lines and remove them
   staveLines = getStaveLines(staveImg);
   staveLineHeight = getLineHeight(staveLines, staveDist);
   staveImg = removeStave(staveImg, staveLines, staveLineHeight);
   staveCount = staveCount + 1;
   staves(staveCount).img = staveImg;
   staves(staveCount).staveDist = staveDist;
   staves(staveCount).staveLineHeight = staveLineHeight;
   %
   pI(borders(i,1):borders(i, 2), :) = staveImg;
end
if debug ~= 0
    figure;
    for i = 1:staveCount
        subplot(staveCount, 1, i); imshow(staves(i).img);
    end
end
%% Extract components
pI = imbinarize(pI, graythresh(pI));
if debug ~= 0
    figure; imshow(pI);
end
[L,num]=bwlabel(pI);
boxes=regionprops(L,'BoundingBox');
% Filter small components
j=0;
meanStaveDist = mean(extractfield(staves, 'staveDist'));
meanStaveLineHeight = mean(extractfield(staves, 'staveLineHeight'));
wMin = meanStaveDist * 0.5; hMin = meanStaveDist * 0.5;
wMax = meanStaveDist * 10; hMax = meanStaveDist * 10;
for i=1:num
    bx=boxes(i).BoundingBox; bw=bx(3); bh=bx(4);
    if wMin<=bw & hMin<=bh & wMax>=bw & hMax>=bh
        j=j+1;
        boxes(j)=boxes(i);
    end
end
boxes=boxes(1:j); num=j;
padding = 0;

%% Process boxes and generate dataset
datasetSize = 0;
for i = 1:num
    box = boxes(i).BoundingBox;
    bx=max([floor(box(1))-padding 1]); by=max([floor(box(2))-padding 1]);
    bw=ceil(box(3))+2*padding; bh=ceil(box(4))+2*padding;
    bx1=min(bx+bw,w); by1=min(by+bh,h); 
    img = pI(by:by1,bx:bx1);
    datasetSize = datasetSize + 1;
    noteParts = splitNote(img, meanStaveDist, meanStaveLineHeight);
    if length(noteParts) < 2
        boxes(i).isNote = 0;
        dataset(datasetSize).img = img;
        dataset(datasetSize).size = size(img);
    else
        boxes(i).isNote = 1;
        for j=1:length(noteParts)
            dataset(datasetSize).img = noteParts(j).img;
            dataset(datasetSize).size = size(noteParts(j).img);
            datasetSize = datasetSize + 1;
        end
        datasetSize = datasetSize - 1;
    end
end
%% Process notes
if debug ~= 0
    %Display boxes
    figure; imshow(I)
    hold on
    for i=1:num
        bx=boxes(i).BoundingBox;
        x=max([floor(bx(1))-padding 1]); y=max([floor(bx(2))-padding 1]);
        bw=ceil(bx(3))+2*padding; bh=ceil(bx(4))+2*padding;
        if boxes(i).isNote ~= 0
            rectangle('Position', [x y bw bh], 'EdgeColor', 'g');
        else
            rectangle('Position', [x y bw bh], 'EdgeColor', 'r');
        end
    end
    hold off
end
end

