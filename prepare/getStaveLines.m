function [ stave ] = getStaveLines( img )
 [~, W, ~] = size(img);
 se = strel('line',W/15,0); % Magic number
 stave = imopen(img,se);
end

