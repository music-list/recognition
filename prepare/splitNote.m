function [noteParts, debugInfo] = splitNote(note, staveDist, staveLineHeight, varargin)
    %SPLITNOTE Split notes to glyphs
    %
    p = inputParser;
    addRequired(p, 'note');
    addRequired(p, 'staveDist');
    addRequired(p, 'staveLineHeight');
    addOptional(p, 'Debug', 0);
    parse(p, note, staveDist, staveLineHeight, varargin{:});
    debug = p.Results.Debug;
    if debug ~= 0
        debugInfo = {};
        debugInfo.img = note;
    end
    [h, w, ~] = size(note);
    % Use connected components for extract only note
    if ~islogical(note)
        bwNote = imbinarize(note, mean(note(:)));
    else
        bwNote = note;
    end
    CC = bwconncomp(bwNote);
    numPixels = cellfun(@numel,CC.PixelIdxList);
    [~, idx] = max(numPixels);
    onlyNote = zeros(h, w);
    onlyNote(CC.PixelIdxList{idx}) = 1;
    % Check biggest component size: if it's too small, it's definetly not
    % note group
    boxes = regionprops(CC, 'BoundingBox');
    biggestBox = boxes(idx).BoundingBox;
    if biggestBox(4) < staveDist * 2.5
        %return source image
        noteParts(1).img = note;
        if debug ~= 0
            debugInfo.centers = []; 
            debugInfo.rads = [];
            debugInfo.eroded = note;
        end
        return;
    end
    % Looking for glyphs
    erodeSize = staveLineHeight + 1;
    eroded = imopen(padarray(onlyNote, [erodeSize erodeSize]), ones(erodeSize)); %Pad array to correct work with borders
    eroded = eroded(erodeSize+1:end-erodeSize, erodeSize+1:end-erodeSize); %Remove padding
    % Looking for small components of eroded image
    filteredComps = zeros(size(eroded));
    erodedCC = bwconncomp(eroded);
    boxesCC = regionprops(erodedCC);
    for i = 1:length(boxesCC)
        bb = boxesCC(i).BoundingBox;
        if bb(3) <= staveDist * 2
            idx = erodedCC.PixelIdxList{i};
            filteredComps(idx) = eroded(idx);
        end
    end
    r = staveDist*0.5;
    [centers, rads] = findCirclesCorr(filteredComps, r, 10);
    if debug ~= 0
        debugInfo.centers = centers; 
        debugInfo.rads = rads;
        debugInfo.eroded = [filteredComps note];
    end
    if isempty(centers)
        noteParts = [];
        return;
    end
    [centersLength ~] = size(centers);
    if centersLength == 1
        noteParts(1).img = note;
        return;
    end
    % Merge centers
    mergeCenters = [];
    mergeCentersCount = 0;
    for i=1:centersLength
        found = 0;
        for j=1:mergeCentersCount
            if abs(mergeCenters(j).x - centers(i, 2)) < staveDist
                mergeCenters(j).ys(length(mergeCenters(j).ys)+1) = centers(i, 1);
                found = 1;
                break;
            end
        end
        if found ~= 0
            continue;
        end
        % Nothing found - add new
        mergeCentersCount = mergeCentersCount + 1;
        mergeCenters(mergeCentersCount).x = centers(i, 2);
        mergeCenters(mergeCentersCount).ys = [centers(i, 1)];
    end
    % Split note to glyphs
    notePartsCount = 0;
    splitSize = staveDist*1.5;
    for i=1:length(mergeCenters)
        lb = floor(max(mergeCenters(i).x-splitSize, 1));
        rb = floor(min(mergeCenters(i).x+splitSize, w));
        noteVertical = onlyNote(:, lb:rb);
        %Cut
        nonZeroRows = ~all(noteVertical == 0, 2);
        noteVertical = noteVertical(nonZeroRows,:);
        %Shift centers
        yShift = find(nonZeroRows, 1, 'first');
        mergeCenters(i).ys = mergeCenters(i).ys - yShift;
        %Remove small elements
        openSize = staveLineHeight + 1;
        woSmall = imopen(padarray(noteVertical, [openSize openSize]),...%Pad array to correct work with borders
            strel('line', openSize, 0));
        woSmall = woSmall(openSize+1:end-openSize, openSize+1:end-openSize); %Remove padding
        [vh vw] = size(woSmall);
        %Check note structure: central part should have smaller brightness
%         rowsSum = sum(woSmall, 2);
%         brigthnessThresh = mean(rowsSum);
%         if ~all(rowsSum(floor(vh/3):floor(2*vh/3)) < brigthnessThresh)
%             %Not a note part
%             continue;
%         end
        %Split verical parts to separate glyphs
        rows = max(woSmall, [], 2);
        notesPos = mean(mergeCenters(i).ys);
        if vh/2 < notesPos
            %Notes at bottom, extract duration from top
            dF = 1;
            dE = find(~rows, 1, 'first');
            %Add distance
            d = min(min(mergeCenters(i).ys) - dE - staveDist/2, 3*staveDist);
            dE = dE + d;
        else
            %Notes at top, extract duration from bottom
            dF = floor(vh/2) + find(rows(floor(vh/2):end), 1, 'first');
            dE = vh;
            %Add distance
            d = min(dF - max(mergeCenters(i).ys) - staveDist/2, 3*staveDist);
            dF = dF - d;
        end
        duration = noteVertical(floor(dF:dE), :);
        if debug ~= 0
            debugInfo.verticals(i).duration = duration;
            debugInfo.verticals(i).img = woSmall;
        end
        %Perform notes extraction
        for j = 1:length(mergeCenters(i).ys)
            cy = mergeCenters(i).ys(j);
            gx = lb;
            glyphRows = max(floor(cy-staveDist/2), 1):...
                min(floor(cy+staveDist/2), vh);
            glyph = noteVertical(glyphRows, :);
            if vh/2 < notesPos
                glyph = [duration; glyph];
                [gh, gw] = size(glyph);
                gy = min(floor(cy+staveDist/2), vh);
                gy = max(gy - gh + yShift, 1);
            else
                glyph = [glyph; duration];
                [gh, gw] = size(glyph);
                gy = max(floor(cy-staveDist/2), 1);
                gy = gy + yShift;
            end
            notePartsCount = notePartsCount + 1;
            noteParts(notePartsCount).img = glyph;
            glyphBox =[...
                gx, gy,...
                gw, gh,...
            ];
            noteParts(notePartsCount).box = glyphBox;
            if debug ~= 0
                debugInfo.verticals(i).notes(j).img = glyph;
                debugInfo.verticals(i).notes(j).box = glyphBox;
            end
        end
    end
    if notePartsCount == 0
        noteParts(1).img = note;
    end
end

