function [woStave] = removeGrayStave(img, stave, staveLineHeight)
%REMOVEGRAYSTAVE Summary of this function goes here
%   Detailed explanation goes here
woStave = img - stave;
woStave = imclose(woStave, ones(staveLineHeight));
woStave(woStave < 0) = 0;
end

