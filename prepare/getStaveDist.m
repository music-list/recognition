function [ dist, startPoints, endPoints ] = getStaveDist( img, linesCount )
[H, W, ~] = size(img);

img = imbinarize(img, mean(img(:)));
% imgEdges = edge(img);
theta_threshold = 1;

[houghTransform, T, R] = hough(img, 'Theta', [-90:(-90+theta_threshold) (89-theta_threshold):89]);
peakThreshold = W / 2;
nhood = [ceil(H/20) length(T)-1];
nhood = nhood + ~rem(nhood, 2);
peaks = houghpeaks(houghTransform, linesCount, 'threshold', peakThreshold, 'nhood',nhood);

peaksCount = length(peaks);

if peaksCount < linesCount
    dist = 0;
    startPoints = [];
    endPoints = [];
else
    lines = houghlines(img,T,R,peaks,'FillGap',W);
    startPoints = reshape([lines.point1], [2 peaksCount]);
    endPoints = reshape([lines.point2], [2 peaksCount]);
    peaks_rho = [lines.rho];
    [peaks_rho, idx] = sort(peaks_rho);
    startPoints = startPoints(:, idx);
    endPoints = endPoints(:, idx);
    dist = median(diff(peaks_rho));
end
end

