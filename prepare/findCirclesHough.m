function [centers, rads] = findCirclesHough(I, r, maxPeaks, varargin)
%CIRCLEHOUGHTRANSFORM Summary of this function goes here
%   Detailed explanation goes here

p = inputParser;
addRequired(p, 'I');
addRequired(p, 'r');
addRequired(p, 'maxPeaks');
addOptional(p, 'Threshold', 2*pi*r * 0.7);
addOptional(p, 'Debug', 0);
parse(p, I, r, maxPeaks, varargin{:});
debug = p.Results.Debug;
maxThreshold = p.Results.Threshold;
if debug ~= 0
    debugInfo = {};
end
%
[h, w, c] = size(I);
if c == 3
    I = rgb2gray(I);
end
% Iterate pixels
r=floor(r);
xs = (1-r):(w+r);
ys = (1-r):(h+r);
field = zeros(length(ys), length(xs));
cosVal = cosd(1:360);
sinVal = sind(1:360);
for i = 1:h
    for j = 1:w
        % Iterate througth radiuses
        if I(i,j) == 0
            continue;
        end
        oldxc = 0;
        oldyc = 0;
        for thetha=1:360
            xc = floor(j - r * cosVal(thetha) + r);
            yc = floor(i - r * sinVal(thetha) + r);
            if (xc == oldxc) && (yc == oldyc)
                %Prevent voting twice
                continue;
            end
            oldxc = xc;
            oldyc = yc;
            try
                field(yc, xc) = field(yc, xc) + I(i,j);
            catch ME
                rethrow(ME)
            end
        end
    end
end
peaksCount = 0;
centers = zeros(maxPeaks, 2);
rads = ones(maxPeaks, 1) * r;
threshold = max(max(field(:)) * 0.9, maxThreshold);
s = [length(ys) length(xs)];
for i=1:maxPeaks
    [m, ind] = max(field(:));
    if m < threshold
        break;
    end
    peaksCount = peaksCount + 1;
    [y, x] = ind2sub(s, ind);
    centers(peaksCount, :) = [ys(y) xs(x)];
    %Suppress
    supressionRadius = floor(r);
    for sy=-supressionRadius:supressionRadius
        for sx=-supressionRadius:supressionRadius
            ny = max(min(sy+y, length(ys)), 1);
            nx = max(min(sx+x, length(xs)), 1);
            field(ny, nx) = 0;
        end
    end
end
centers = centers(1:peaksCount, :);
rads = rads(1:peaksCount);
end

