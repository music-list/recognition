function [centers, rads] = findCirclesCorr(I, r, maxPeaks, varargin)
%CIRCLEHOUGHTRANSFORM Summary of this function goes here
%   Detailed explanation goes here

p = inputParser;
addRequired(p, 'I');
addRequired(p, 'r');
addRequired(p, 'maxPeaks');
addOptional(p, 'Threshold', (0.8*pi*r^2));
parse(p, I, r, maxPeaks, varargin{:});
maxThreshold = p.Results.Threshold;
%
[h, w, c] = size(I);
if c == 3
    I = rgb2gray(I);
end
% Peroform correlation
r=floor(r);
disk = double(fspecial('disk', r) > 0);
I = double(I > 0);
field = conv2(I, disk', 'same');
% Looking for peaks
peaksCount = 0;
centers = zeros(maxPeaks, 2);
rads = ones(maxPeaks, 1) * r;
threshold = max(max(field(:)) * 0.8, maxThreshold);
s = size(field);
for i=1:maxPeaks
    [m, ind] = max(field(:));
    if m < threshold
        break;
    end
    peaksCount = peaksCount + 1;
    [y, x] = ind2sub(s, ind);
    centers(peaksCount, :) = [y x];
    %Suppress
    supressionRadius = floor(r*1.5);
    for sy=-supressionRadius:supressionRadius
        for sx=-supressionRadius:supressionRadius
            ny = max(min(sy+y, s(1)), 1);
            nx = max(min(sx+x, s(2)), 1);
            field(ny, nx) = 0;
        end
    end
end
centers = centers(1:peaksCount, :);
rads = rads(1:peaksCount);
end

