function [ borders ] = getBorders(bw, varargin)
    [H, W, ~] = size(bw);
    %% Parse input
    p = inputParser;
    addRequired(p, 'bw');
    addOptional(p, 'DilateSize', W/40);
    addOptional(p, 'Expand', 0.2);
    parse(p, bw, varargin{:});
    % Minimal size of stave line size
    line_size = p.Results.DilateSize;
    % Size of region
    expand_coeff = p.Results.Expand;
    
    if line_size > 0
        se = strel('line', line_size, 90);
        dilated = imdilate(bw,se);
    else
        dilated = bw;
    end
    
    image_middle = mean(dilated(:));
    threshold = image_middle;
    row_middle = mean(dilated, 2);

    % Looking for intensity over threshold
    % Compute gradient
    % Looking for non-zero indexes
    rows_gradient = diff(row_middle > threshold);
    starts = find(rows_gradient > 0);
    ends = find(rows_gradient < 0);
    if starts(1) > ends(1)
        ends = ends(2:1:length(ends));
    end
    if starts(end) > ends(end)
        starts = starts(1:end-1);
    end
    sizes = ends - starts;
    starts = ceil(starts - sizes * expand_coeff);
    starts(starts<1) = 1;
    ends = ceil(ends + sizes * expand_coeff);
    ends(ends>H) = H;
    borders = [starts, ends];
end

