close all; clear all;
addpath 'utils'; addpath 'clustering';
filename = 'test1.png';
load(['target/' 'splitted-' filename '.mat'], 'imgs', 'meanStaveDist');
%% Count distances
sizes = zeros(length(imgs), 2);
for i=1:length(imgs)
    notes{i} = imgs(i).img;
    sizes(i, :) = imgs(i).size;
end
distances = countImgDistance(notes, 'Debug', 1);
%% Perform clustering
links = linkage(distances, 'ward');
figure; dendrogram(links,100);
%Split
inc = inconsistent(links);
clusters = cluster(links, 'cutoff', max(inc(:, 4)) * 0.9);
%% Display clusters
clustersCount = max(clusters);
clustersSize = accumarray(clusters, 1);
maxSize = max(sizes, [], 1);
out = zeros(maxSize(1) * clustersCount, maxSize(2) * max(clustersSize));
xPerCluster = ones(clustersCount, 1);
for i = 1:length(imgs)
    img = imgs(i).img;
    currentCluster = clusters(i);
    x = xPerCluster(currentCluster);
    y = (currentCluster - 1) * maxSize(1) + 1;
    out(y:(y+imgs(i).size(1)-1), x:(x+imgs(i).size(2))-1) = img;
    xPerCluster(currentCluster) = xPerCluster(currentCluster) + maxSize(2);
end
figure; imshow(out);