function [ juncs ] = get_notejunc( img, dist )
[ih, iw, ~] = size(img);

angle_border = 30;
fm = zeros(ih, iw);
for ang=-angle_border:5:angle_border
    if ang~=0
        s=strel('line', dist*2, ang);
        s=strel(imdilate(getnhood(s),ones(ceil(dist/4),1)));
        f1=imopen(img,s);
        idx=f1>fm; fm(idx)=f1(idx);
    end
end;

juncs = fm;
end

