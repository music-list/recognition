function [ lines ] = get_vl( img, stave_dist)
    lines = imopen(img, ones(ceil(stave_dist), 1));
end

