function [ nim ] = extractImage( im,bndBoxes,xMargin,yMargin,index)
    % Возвращает образцы, вырезанные из обрабатываемого изображения im.
    %  bndBoxes - границы образцов;
    %  xMargin,yMargin - поля для расширения области образцов;
    %  index - сквозной индекс образца 1:length(bndBoxes).
    bx=bndBoxes(index).BoundingBox;
    x=max([floor(bx(1))-xMargin 1]); y=max([floor(bx(2))-yMargin 1]);
    w=ceil(bx(3))+2*xMargin; h=ceil(bx(4))+2*yMargin;
    [hg,wd]=size(im);
    x1=min(x+w,wd); y1=min(y+h,hg); 
    note=im(y:y1,x:x1);
    sz=cat(1,bndBoxes.BoundingBox);
    maxWidth=ceil(max(sz(:,3)))+2*xMargin+1; % макс.размер образца
    maxHeight=ceil(max(sz(:,4)))+2*yMargin+1;
    padVal=max(note(:)); % используется для полей
    nim=repmat(padVal,maxHeight,maxWidth);
    nim(1:size(note,1),1:size(note,2),:)=note;
end

