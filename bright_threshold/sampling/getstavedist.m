function [ dist ] = getstavedist( img, lines_count )
[H, W, ~] = size(img);

edges = edge(img);
theta_threshold = 1;

[houghTransform, T, R] = hough(edges, 'Theta', [-90:(-90+theta_threshold) (89-theta_threshold):89]);
peakCount = lines_count;
peakThreshold = W / 8;
nhood = [ceil(H/20) length(T)-1];
nhood = nhood + ~rem(nhood, 2);
peaks = houghpeaks(houghTransform, peakCount, 'threshold', peakThreshold, 'nhood',nhood);

peaksCount = length(peaks);

if peaksCount < lines_count
    dist = 0;
else
    lines = houghlines(img,T,R,peaks,'FillGap',W);
    xy = [lines.point1];
    peaks_rho = [lines.rho];
    peaks_rho = sort(peaks_rho);
    dist = median(diff(peaks_rho));
end;
end

