function [ wo_sticks ] = remove_note_sticks( wo_stave, stave_dist, line_height)
    %Получаем ноты
    [ih, iw, ~] = size(wo_stave);
    min_size = stave_dist*2+1;
    max_size = stave_dist*3;
    se = strel('disk', ceil(stave_dist/4), 0);
    notes = imopen(wo_stave, se);
    note_lines = get_vl(notes, min_size);
    %Получаем препарат для поиска линий, соединяющих ноты.
    %Для этого выбираем только линии длиной от двух до трех расстояний
    %И вычитаем из них линии, который могут быть найдены в нотах
    big_lines = get_vl(wo_stave, max_size);
    small_lines = get_vl(wo_stave, min_size);
    diff = small_lines - big_lines - note_lines;
    %Выбираем только линии, которые соприкасаются с нотами.
    filtered = imreconstruct(notes, logical(diff));
    to_remove = big_lines|filtered;
    wo_sticks = wo_stave;
    close_sample = strel('line', ceil(line_height), 0);
    for k=-line_height:line_height
        shift_to_remove = to_remove;
        if k == 0
            shift_to_remove = to_remove;
        end;
        if k > 0
            shift_to_remove = [to_remove(:, (1+k):iw) zeros(ih,k)];
        end;
        if k < 0
            shift_to_remove = [zeros(ih, -k) to_remove(:,1:iw+k)];
        end;
        wo_shifted_sticks = wo_stave - shift_to_remove;    
        remove_fail = imclose(wo_shifted_sticks, close_sample)&shift_to_remove;
        wo_shifted_sticks = wo_shifted_sticks|remove_fail;
        wo_sticks = wo_sticks&wo_shifted_sticks;
    end;
end

