function [ wostave ] = remove_stave( img, stave, line_h )
img = im2bw(img);
[ih, iw, ~] = size(img);

f1=img&~stave;
close_h = ceil(line_h*2);
f1=f1|imclose(f1,ones(close_h, 1))&stave;
it = line_h;
for k=-it:it
    shift_stave = stave;
    if k == 0
        continue;
    end;
    if k > 0
        shift_stave = [stave((1+k):ih, :); zeros(k,iw)];
    end;
    if k < 0
        shift_stave = [zeros(-k,iw); stave(1:ih+k,:)];
    end;
    f2=img&~shift_stave;
    df = imclose(f2,ones(close_h))&shift_stave;
    f2=f2|df;
    f1=f1&f2;   
end;
wostave = f1;
end

