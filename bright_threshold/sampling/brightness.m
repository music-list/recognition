close all;
clear all;
img_file = '../test1.png';
img = imread(img_file);
delete 'stave*.png';
f=double(img); 
[H, W, c] = size(f);
tmp_mat = zeros(H, W);
for i=1:c
    tmp_mat = tmp_mat + f(:, :, i);
end
f=mat2gray(tmp_mat);
negative = mat2gray(-f);
stave_size = 5;

borders = getborders(negative);
% figure; imshow(negative);
% hold on;
% plot([0 W], [borders(:, 1) borders(:, 1)], 'g');
% plot([0 W], [borders(:, 2) borders(:, 2)], 'r');
% hold off;

prepared = negative;
sd_list = zeros(length(borders));
for k = 1:size(borders)    
    part = negative(borders(k,1):borders(k, 2), :);
    width = borders(k, 2) - borders(k,1);
    if width < H/(length(borders)*2)
        continue;
    end
    sd = getstavedist(part, stave_size);
    imwrite(negative(borders(k,1):1:borders(k, 2), :), ['stave' num2str(k) '.png']);
    if sd > 0
        only_stave = getstave(part);
        lh = line_height(only_stave, ceil(sd));
        sd_list(k) = sd;
        
        tmp_channel = remove_stave(part, only_stave, lh);
        %figure; imshow(tmp_channel);
        tmp_channel = remove_note_sticks(tmp_channel, sd, lh);
        prepared(borders(k,1):1:borders(k, 2), :) = tmp_channel;
        
    end;
end
figure; imshow(prepared);

% Обработка связных компонент
[L,num]=bwlabel(prepared);
num
boxes=regionprops(L,'BoundingBox'); % ограничивающие рамки
j=0;
dm = 22;
wmin=dm*0.5; wmax=dm*4; hmin=dm*0.5; hmax=dm*4; % допуски на размеры
for i=1:num
    bx=boxes(i).BoundingBox; w=bx(3); h=bx(4);
    if wmin<=w & w<=wmax & hmin<=h & h<=hmax
        j=j+1;
        boxes(j)=boxes(i);
    end
end
boxes=boxes(1:j); num=j;
CC=logical(zeros(H,W));
mrg=3; % поле расширения рамки
for i=1:num
    bx=boxes(i).BoundingBox;
    x=max([floor(bx(1))-mrg 1]); y=max([floor(bx(2))-mrg 1]);
    w=ceil(bx(3))+2*mrg; h=ceil(bx(4))+2*mrg;
    x1=min(x+w,W); y1=min(y+h,H); 
    CC(y,x:x1)=1; CC(y1,x:x1)=1;
    CC(y:y1,x)=1; CC(y:y1,x1)=1;
end
% Показ результата: рамки красным цветом.
clr=zeros(H,W,3);
f1=negative; f1(CC)=1; clr(:,:,1)=f1;
f1=negative; f1(CC)=0; clr(:,:,2)=f1;
f1=negative; f1(CC)=0; clr(:,:,3)=f1;
figure; imshow(clr);

sd = mean(sd_list(sd_list > 0));

save 'notes.mat' boxes mrg img_file sd;