function [ height ] = line_height( stave, line_dist )
maxline_h = ceil(line_dist/2);
result = zeros(maxline_h, 1);
for k=1:maxline_h
    line_ex = ones(k, 1);
    opened = imopen(stave, line_ex) > 0;
    result(k) = sum(sum(opened));
end;
[~, idx] = max(-diff(result));
height = idx;
end

