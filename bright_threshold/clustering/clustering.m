echo off
close all;
clear all;

load 'notes.mat';
box = boxes;
load 'notesDist.mat';
img=imread(img_file);
f=double(img); 
[hg,wd,cn]=size(img);
tmp_mat = zeros(hg, wd);
for i=1:cn
    tmp_mat = tmp_mat + f(:, :, i);
end;
f=mat2gray(tmp_mat);
negative = mat2gray(-f);

% Построение иерархии методом:
% 'single'   --- минимального локального расстояния
% 'complete' --- максимального локального расстояния
% 'average'  --- среднего расстояния
% 'centroid' --- центроидным
% 'ward'     --- Уорда, внутригрупповая сумма квадратов (ВСК)
links=linkage(dist,'ward');

figure; dendrogram(links,100); % дендограмма
c = cophenet(links,dist) % cophenetic correlation coefficient
inc = inconsistent(links);

% Определение кластеров:
% - по коэффициенту несовместимости (inconsistency coefficient)
% clust=cluster(links,'cutoff',1.15);
% - по уровню дендограммы
% clust=cluster(links,'cutoff',80,'criterion','distance');
% - по максимальному количеству кластеров
% clust=cluster(links,'maxclust',20);
max_inc = max(inc(:, 4));
clust = cluster(links, 'cutoff', max_inc*0.95);

nclust=max(clust); % количество кластеров
cntClust=zeros(1,nclust); % количество образцов в каждом кластере
for i=1:nclust
    cntClust(i)=sum(clust==i);
end
note1=extractImage(f,box,mrg,mrg,1);
sizeNote=size(note1); % макс.размер изображения образца
out=uint8(zeros(sizeNote(1).*nclust,sizeNote(2).*max(cntClust)));
x=ones(1,nclust);
y=[0:nclust-1].*sizeNote(1)+1;
[~,idx]=sort(max(cntClust)-cntClust); % сорт.по размеру кластера
srt=sortrows([idx' y'],1);
y=srt(:,2)';
for i=1:length(box) % перебор всех образцов
    note1=extractImage(f,box,mrg,mrg,i);
    nc=clust(i);
    x1=x(nc); x2=x1+sizeNote(2);
    y1=y(nc); y2=y1+sizeNote(1)-1;
    out(y1:y2,x1:x2-1)=uint8(round(note1.*255));
    x(nc)=x2;
end
figure; imshow(out);
[~, name, ~] = fileparts(img_file);
save ([name '.clust.mat'], 'img_file', 'clust', 'boxes', 'mrg', 'img', 'sd');