function varargout = clusterHelper(varargin)
% CLUSTERHELPER MATLAB code for clusterHelper.fig
%      CLUSTERHELPER, by itself, creates a new CLUSTERHELPER or raises the existing
%      singleton*.
%
%      H = CLUSTERHELPER returns the handle to a new CLUSTERHELPER or the handle to
%      the existing singleton*.
%
%      CLUSTERHELPER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CLUSTERHELPER.M with the given input arguments.
%
%      CLUSTERHELPER('Property','Value',...) creates a new CLUSTERHELPER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before clusterHelper_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to clusterHelper_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help clusterHelper

% Last Modified by GUIDE v2.5 10-Jun-2017 20:29:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @clusterHelper_OpeningFcn, ...
                   'gui_OutputFcn',  @clusterHelper_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before clusterHelper is made visible.
function clusterHelper_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;

load 'types.mat';
handles.types = types;

t = strcat(types, '\n');
s = sprintf(strcat(t{:}));
set(handles.typesselect, 'String', s);


guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = clusterHelper_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function clusters_CreateFcn(hObject, eventdata, handles)
% hObject    handle to clusters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in samples.
function samples_Callback(hObject, eventdata, handles)
% hObject    handle to samples (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns samples contents as cell array
%        contents{get(hObject,'Value')} returns selected item from samples


% --- Executes during object creation, after setting all properties.
function samples_CreateFcn(hObject, eventdata, handles)
% hObject    handle to samples (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in removebutton.
function removebutton_Callback(hObject, eventdata, handles)
sample_num = get(handles.samples, 'Value');
cluster_num = get(handles.clusters, 'Value') - 1;
idx = find(handles.clust == cluster_num);
sample_id = idx(sample_num);
clust = handles.clust;
clust(sample_id) = 0;
handles.clust = clust;
guidata(handles.removebutton, handles);
updateSamples(handles, cluster_num);

% --- Executes on button press in addbutton.
function addbutton_Callback(hObject, eventdata, handles)
cluster_num = 0; %Кластер с нераспределенными значениями
new_cluster_num = get(handles.clusters, 'Value') - 1;
idx = find(handles.clust == cluster_num);
sample_id = str2num(get(handles.idtext, 'String'));
if isempty(find(idx == sample_id, 1))
    msgbox('Образец с таким ID уже определен в кластер');
else
    clust = handles.clust;
    clust(sample_id) = new_cluster_num;
    handles.clust = clust;
    guidata(handles.addbutton, handles);
    updateSamples(handles, new_cluster_num);
end

% --- Executes on button press in result.
function result_Callback(hObject, eventdata, handles)
tmpMarks = handles.cluster_marks;
tmpClusters = handles.clust;
tmpName = handles.img_file;
[~, name, ~] = fileparts(tmpName);
save ([name '.mark.mat'], 'tmpName', 'tmpMarks', 'tmpClusters');
msgbox('Сохранено');



function idtext_Callback(hObject, eventdata, handles)
% hObject    handle to idtext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of idtext as text
%        str2double(get(hObject,'String')) returns contents of idtext as a double


% --- Executes during object creation, after setting all properties.
function idtext_CreateFcn(hObject, eventdata, handles)
% hObject    handle to idtext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in clusters.
function clusters_Callback(hObject, eventdata, handles)
% hObject    handle to clusters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cluster_num = get(hObject,'Value') - 1;
if cluster_num ~= 0    
    cluster_mark = handles.cluster_marks(cluster_num);
    set(handles.typesselect, 'Value', cluster_mark);
end;
guidata(handles.typesselect, handles);
updateSamples(handles, cluster_num);

function updateSamples(handles, cluster_num)
clust = handles.clust;
idx = find(clust == cluster_num);
set(handles.samples, 'String', idx);
if length(idx) ~= 0
    set(handles.samples, 'Value', 1);
    displayClusterImage(idx, handles);
else    
    set(handles.samples, 'Value', []);
end;

function displayClusterImage(idx, handles)
%Generate cluster image
f = handles.img;
box = handles.boxes;
mrg = handles.mrg;
note1 = extractImage(f,box,mrg,mrg,idx(1));
[h, w, ~] = size(note1);
axes_width = handles.axes1.OuterPosition(3) * 10;
max_idx_x = floor(axes_width/w);
img = zeros(h * ceil(length(idx) / max_idx_x), w*max_idx_x);
idx_y = 1; idx_x = 1;
for k=1:length(idx)
    note1 = extractImage(f,box,mrg,mrg,idx(k));
    y = (idx_y-1)*h+1; y1 = y + h - 1;
    x = (idx_x-1)*w+1; x1 = x + w - 1;
    img(y:y1, x:x1) = note1;
    idx_x = idx_x + 1;
    if idx_x > max_idx_x
        idx_x = 1;
        idx_y = idx_y + 1;
    end;
end;
axes(handles.axes1);
imshow(img)


% --- Executes on selection change in typesselect.
function typesselect_Callback(hObject, eventdata, handles)
cluster_mark = get(hObject,'Value');
cluster_num = get(handles.clusters, 'Value') - 1;
handles.cluster_marks(cluster_num) = cluster_mark;
guidata(handles.typesselect, handles);



% --- Executes during object creation, after setting all properties.
function typesselect_CreateFcn(hObject, eventdata, handles)
% hObject    handle to typesselect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in loadmarks.
function loadmarks_Callback(hObject, eventdata, handles)
filename = uigetfile('*.mark.mat');
S = load(filename);
if S.tmpName ~= handles.img_file
    msgbox('Выберите метки, соответствующие файлу');
    return;
end;
handles.cluster_marks = S.tmpMarks;
cluster_num = get(handles.clusters,'Value') - 1;
if cluster_num ~= 0    
    cluster_mark = handles.cluster_marks(cluster_num);
    set(handles.typesselect, 'Value', cluster_mark);
end;
guidata(handles.loadmarks, handles);

% --- Executes on button press in loadclusters.
function loadclusters_Callback(hObject, eventdata, handles)
filename = uigetfile('*.clust.mat');
load(filename);
handles.boxes = boxes;
handles.mrg = mrg;
handles.img_file = img_file;
handles.sd = sd;
f=double(img); 
[H, W, c] = size(f);
tmp_mat = zeros(H, W);
for i=1:c
    tmp_mat = tmp_mat + f(:, :, i);
end;
f=mat2gray(tmp_mat);
negative = mat2gray(-f);
handles.img = f;
handles.negative = negative;
handles.clust = clust;

%Generate list content
clusters = unique(clust);
handles.cluster_marks = ones(1, length(clusters));
set(handles.clusters, 'String', [0; clusters]);
guidata(handles.loadclusters, handles);


% --- Executes on button press in savesamples.
function savesamples_Callback(hObject, eventdata, handles)
clust = handles.clust;
marks = handles.cluster_marks;
clustCount = length(marks);
mrg = handles.mrg;
img = handles.img;
medianCnt = 1;
sd = handles.sd;
wCoeff = zeros(1, clustCount);
hCoeff = zeros(1, clustCount);
for k=1:clustCount
    if marks(k) == 0
        continue;
    end
    idx = find(clust == k);
    idxl = length(idx);
    bx = [handles.boxes(idx).BoundingBox];
    w = floor(max(bx(3:4:length(bx))));
    h = floor(max(bx(4:4:length(bx))));    
    x = floor(bx(1)); y = floor(bx(2));
    median = img(y:(y+h-1),x:(x+w-1))./idxl;
    for j=2:length(idx)
        x =floor(bx(1 + 4*(j-1))); y = floor(bx(2 + 4*(j-1)));
        currentImg = img(y:(y+h-1),x:(x+w-1))./idxl;
        median = median + currentImg;
    end;
    wCoeff(k) = w / handles.sd;
    hCoeff(k) = h / handles.sd;
    medians{medianCnt} = median;
    medianCnt = medianCnt  + 1;
end;
wc = num2cell(wCoeff(find(marks > 0)));
hc = num2cell(hCoeff(find(marks > 0)));
mark = num2cell(marks(find(marks > 0)));
s = struct('mark', mark, 'median', medians, 'wc', wc, 'hc', hc, 'sd', sd);
[~, name, ~] = fileparts(handles.img_file);
save ([name '.samples.mat'], 's');
msgbox('Сохранено');