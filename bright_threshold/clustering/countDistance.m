echo off
% Расчет матрицы рассто€ний; dist (суммы квадратов отклонений)
close all
clear all
tic;

load 'notes.mat';
box = boxes;
img=imread(img_file);
f=double(img); 
[hg,wd,cn]=size(img);
tmp_mat = zeros(hg, wd);
for i=1:cn
    tmp_mat = tmp_mat + f(:, :, i);
end;
f=mat2gray(tmp_mat);
negative = mat2gray(-f);

cnt = ceil(length(box));


for i=1:cnt
    images{i} = extractImage(f,box,mrg,mrg,i);
    negatated{i} = mat2gray(-images{i});%формируем вектор негативов
end

dist=zeros(1, cnt*(cnt-1)/2);
small_dist = zeros(1, cnt*(cnt-1)/2);
sqr=zeros(1,cnt); % матрица дисперсий
for i=1:cnt
    note1=negatated{i};
    sqr(i)=sum(sum(note1.^2)); % нормировка на диапазон [0,1]
end
wtb=waitbar(0,'Please wait...','CreateCancelBtn',...
    'setappdata(gcbf,''canceling'',1)');
setappdata(wtb,'canceling',0)
set(wtb,'Name','Clustering Progress');
k=1;
for i=1:cnt-1 % перебор всех пар образцов
    note1=negatated{i};
    for j=i+1:cnt
        note2=negatated{j};
        corr=xcorr2(note1, note2); % корреляция        
        m=max(max(corr)); % максимум корреляции
        dist(k)=sqr(i)+sqr(j)-2.*m; % сумма квадратов отклонений
        k=k+1;
    end
    if getappdata(wtb,'canceling')
        break
    end
    waitbar(k/length(dist),wtb);
end
delete(wtb);
save 'notesDist.mat' dist;
toc
