close all; clear all;

load 'colors.mat';
load 'types.mat';
type_count = length(types);

filename = '../test1.png';
img = imread(filename);
f=double(img); 
[h, w, c] = size(f);
marksize = floor(min(w, h) / 200);
tmp_mat = zeros(h, w);
stave_size = 5;
threshold = 0.5;
mark_type = 4;
for i=1:c
    tmp_mat = tmp_mat + f(:, :, i);
end;
f=mat2gray(tmp_mat);
negative = mat2gray(-f);

samples_list = ['test3.samples.mat'; 'test1.samples.mat'];
[ls, ~] = size(samples_list);
samples = [];
for k=1:ls
    sample_name = samples_list(k, :);
    s = load(sample_name);
    new_samples = s.s;
    samples =  [samples new_samples];
end;

clr = zeros(h, w, 3);
clr(:, :, 1) = negative; clr(:, :, 2) = negative; clr(:, :, 3) = negative;

wtb=waitbar(0,'Please wait...','CreateCancelBtn',...
            'setappdata(gcbf,''canceling'',1)');
setappdata(wtb,'canceling',0);

borders = getborders(negative);
for b = 1:size(borders)
    part = negative(borders(b, 1):borders(b, 2), :);    
    [ph, pw] = size(part);
    if ph < h/(length(borders)*2)
        continue;
    end;
    
    sd = getstavedist(part, stave_size);    
    if sd > 0
        part_clr = zeros(ph, pw, 3);
        part_clr(:, :, 1) = part; part_clr(:, :, 2) = part; part_clr(:, :, 3) = part;
       
        set(wtb,'Name','Recognition Progress');
        %Итерация по всем образцам. Максимумы для каждой метки записываются в
        %массив mark_corr
        mark_corr = zeros(ph, pw, type_count);
        for k=1:length(samples)
            sample = mat2gray(-samples(k).median);
            scale = sd / samples(k).sd;
            sample = imresize(sample, scale);
            mark = samples(k).mark;
            corr = normxcorr2(sample, part);
            [sample_h, sample_w] = size(sample);
            x = floor(sample_w / 2); y = floor(sample_h / 2);
            x1 = x + pw - 1; y1 = y + ph - 1;
            mark_corr(:, :, mark) = max(corr(y:y1,x:x1), mark_corr(:, :, mark));

            if getappdata(wtb,'canceling')
                break
            end
            waitbar((k + (b-1)*length(samples))/(length(samples)*length(borders)),wtb);
        end;
        
        set(wtb,'Name','Display Progress');
        filtered_corr = mark_corr;
        filtered_corr(filtered_corr < threshold) = 0;
        
        [max_corr, marks] = max(filtered_corr, [], 3);
        max_peaks = imregionalmax(max_corr);        
        marks(~max_peaks) = 0;
        
        suppressSize = ceil(sd);
        while 1 == 1
            [col_max, col_idx] = max(max_corr);
            [maxc, idx] = max(col_max);
            if maxc == 0
                break;
            end;
            i = col_idx(idx);
            j = idx;
            x = max([j-floor(suppressSize/2) 1]);
            y = max([i-floor(suppressSize/2) 1]);
            x1 = min([j+floor(suppressSize/2)-1 pw]);
            y1 = min([i+floor(suppressSize/2)-1 ph]);
            area = max_corr(y:y1, x:x1);
            mark_area = marks(y:y1, x:x1);
            areaMax = max(max(area));
            %Suppress
            mark_area(area < areaMax) = 0;
            max_corr(y:y1, x:x1) = 0;
            marks(y:y1, x:x1) = mark_area;
        end;

        for i=1:ph
            for j=1:pw
                if (marks(i, j) > 2)
                    x = max([j-floor(marksize/2) 1]); y = max([i-floor(marksize/2) 1]);            
                    x1 = min([x+marksize-1 pw]); y1 = min([y+marksize-1 ph]);
                    color = colors(marks(i, j), :);
                    for i_m=y:y1
                        for j_m=x:x1
                            part_clr(i_m, j_m, :) = color;
                        end;
                    end;
                end;
            end;
            if getappdata(wtb,'canceling')
                break
            end
            waitbar((i*w + (b-1)*h*w)/(length(borders)*h*w), wtb);
        end;
        %figure; imshow(part_clr);
        imwrite(part_clr, ['recognized' num2str(b) '.png']);
        clr(borders(b, 1):borders(b, 2), :, :) = part_clr;
    end;
end

delete(wtb);

figure; imshow(clr); title(types{mark_type});