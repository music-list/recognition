function [targetDir] = initTarget()
    %initTarget Return target directory name and create new, if required
    targetDir = ['target' '\']; %Should ended with slash
    if ~exist(targetDir, 'dir')
        disp('initTarget: Target directory does not exist, create new')
        mkdir(targetDir);
    end
end

