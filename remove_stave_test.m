clear all; close all
addpath 'prepare';
addpath 'utils';
lh = 3;
staveImg = imread(['target/' 'staveimg.png']);
staveLines = imread(['target/' 'stavelines.png']);
%Check
I = removeStave(staveImg, staveLines, lh);
imwrite(I, ['target/' 'wostavelines.png']);
figure; imshow(I);