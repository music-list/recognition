clear all
%% Load Constants
fs = 16000;
load('notes.mat');
%% Load file
filename = 'lighting_retinex.png';
load(['target/' filename '.sounds.mat'], 'staveSounds');
staveCount = length(staveSounds);
totalTime = 0;
currentTime = 0;
for i = 1:staveCount
    currentSounds = staveSounds(i).sounds;
    soundsCount = length(currentSounds);
    for j = 1:soundsCount
        totalTime = totalTime + currentSounds(j).duration;
    end
end
disp(['Total time: ' num2str(totalTime) ' seconds']);
%% Add some sound
time = 0:1/fs:totalTime;
result = zeros(1, length(time));
for i = 1:staveCount
    currentSounds = staveSounds(i).sounds;
    soundsCount = length(currentSounds);
    for j = 1:soundsCount
        currentSound = currentSounds(j);
        startTime = currentTime;
        currentTime = currentTime + currentSound.duration;
        endTime = currentTime;
        timeIdx = (startTime*fs):(endTime*fs);
        timeIdx = timeIdx + 1;
        noteCount = length(currentSound.notes);
        for k = 1:length(noteCount)
            currentNote = currentSound.notes(k);
            noteNumber = currentNote.octave * octaveSize + clearNotes(currentNote.clearNote);
            freq = notesFreq(noteNumber);
            soundPart = sin(2*pi*freq*time(timeIdx));
            result(timeIdx) = result(timeIdx) + soundPart;
        end
    end
end
%% Play sound
audiowrite(['target/' filename '.wav'], result, fs);
sound(result, fs);
disp('Finished!');
% t1=[0:1/fs:0.3];
% t2=[0.3:1/fs:0.6];
% t3=[0.6:1/fs:0.9];
% C2 = 523.26;
% D2 = 554.36;
% F2 = 698.46;
% notes = [C2*t1 D2*t2 C2*t3];
% play1 = sin(2*pi*notes);
% sound(play1,fs);