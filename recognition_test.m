close all; clear all;
debug = 1;
addpath('prepare');
addpath('recognition');
addpath('utils');
%% Types
load('types.mat');
%% Notes info
load('notes.mat');
%% Load clusters
load('gui/test2.mdb.mat');
clustersList = prepareClusters(clustersList);
%% Load image
filename = 'lighting_retinex.png';
I = imread(['images/' filename]);
%% Prepare image
[h, w, c] = size(I);
if c == 3
    I = rgb2gray(I);
end
I = double(I);
I = mat2gray(-I);
%% Split into staves
% Get borders of staves
staveCount = 0;
borders = getBorders(I);
if debug ~= 0
    figure; imshow(I);
    hold on;
    plot([0 w], [borders(:, 1) borders(:, 1)], 'g');
    plot([0 w], [borders(:, 2) borders(:, 2)], 'r');
    hold off;
end
% Prepare image for extraction
baseLine = 0;
baseNote = 0;
baseOctave = 0;
% Extract and filter staves with notes
for i = 1:length(borders)
    staveImg = I(borders(i,1):borders(i, 2), :);
    [sh, sw, ~] = size(staveImg);
    [staveDist, staveLinesStarts, staveLinesEnds] = getStaveDist(staveImg, 5);
    if staveDist == 0
       continue
    end
    staveCount = staveCount + 1;
    staveDist = round(staveDist);
    staveLines = getStaveLines(staveImg);
    staveLinesHeight = getLineHeight(staveLines, staveDist);
    staveImg = removeGrayStave(staveImg, staveLines, staveLinesHeight); %Removes background and stave
    staveImgClosed = imclose(imbinarize(staveImg), ones(staveDist, 1)); %Removes small gaps in notes
    % Looking for components
    [L, num]=bwlabel(staveImgClosed);
    boxes=regionprops(L,'BoundingBox');
    % Filter small components
    filtered=0;
    wMin = staveDist * 0.5; hMin = staveDist * 0.5;
    wMax = staveDist * 10; hMax = staveDist * 10;
    for j=1:num
        box=boxes(j).BoundingBox;
        bw=box(3); bh=box(4);
        if wMin<=bw && hMin<=bh && wMax>=bw && hMax>=bh
            filtered=filtered + 1;
            boxes(filtered)=boxes(j);
        end
    end
    boxes=boxes(1:filtered); num=filtered;
    % Extract and split components
    componentsCount = 0;
    components = struct;
    for j = 1:num
        box = boxes(j).BoundingBox;
        x = floor(max(box(1), 1)); xEnd = floor(min(x + box(3) - 1, sw));
        y = floor(max(box(2), 1)); yEnd = floor(min(y + box(4) - 1, sh));
        img = staveImg(y:yEnd, x:xEnd);
        %Check notes group
        noteParts = splitNote(img, staveDist, staveLinesHeight);
        if length(noteParts) > 1
            for k=1:length(noteParts)
                noteBox = noteParts(k).box;
                noteBox = noteBox + [box(1) box(2) 0 0];
                noteImg = noteParts(k).img;
                componentsCount = componentsCount + 1;
                components(componentsCount).img = noteImg;
                components(componentsCount).box = noteBox;
                components(componentsCount).parentBox = box;
            end
        else
            componentsCount = componentsCount + 1;
            components(componentsCount).img = img;
            components(componentsCount).box = box;
        end
    end
    % Work with components
    if debug ~= 0
        onlyComponents = zeros(size(staveImg));
    end
    recognisedComponentCount = 0;
    for j=1:componentsCount
        img = components(j).img;
        box = components(j).box;
        if debug ~= 0
            x = floor(max(box(1), 1)); xEnd = floor(min(x + box(3) - 1, sw));
            y = floor(max(box(2), 1)); yEnd = floor(min(y + box(4) - 1, sh));
            onlyComponents(y:yEnd, x:xEnd) = img;
        end
        [class, classesMax] = calculateClass(img, staveDist,...
            clustersList, 'Types', types);
        components(j).class = class;
        components(j).correlation = classesMax(classesMax(:, 1) == class, 2);
        if class ~= 1 %If recognised
            components(j).center = getCenter(img, class, staveDist);
            components(j).absCenter = [box(2) box(1)] + components(j).center;
            recognisedComponentCount = recognisedComponentCount + 1;
            recognisedComponents(recognisedComponentCount) = components(j);
            recognisedIdx(recognisedComponentCount) = j;
        end
    end
    %Sort components
    componentsXPos = zeros(recognisedComponentCount, 1);
    for j=1:recognisedComponentCount
        componentsXPos(j) = recognisedComponents(j).absCenter(2);
    end
    [sorted, idx] = sort(componentsXPos);
    recognisedIdx = recognisedIdx(idx);
    recognisedComponents = recognisedComponents(idx);
    %Looking for first clef
    for j=1:recognisedComponentCount
        class = recognisedComponents(j).class;
        minDist = sh;
        if class == 11 || class == 10
            %Search for nearst line
            for k=1:length(staveLinesStarts)
                lp1 = [staveLinesStarts(2, k) staveLinesStarts(1, k)];
                lp2 = [staveLinesEnds(2, k) staveLinesEnds(1, k)];
                p = recognisedComponents(j).absCenter;
                %
                dist = abs(linePointDistance(p, lp1, lp2));
                if dist < minDist
                    minDist = dist;
                    baseLine = k;
                end
            end
            if class == 11
                baseNote = 1;
                baseOctave = 5;
            end
            break;
        end
    end
    baseLineStart = [staveLinesStarts(2, baseLine) staveLinesStarts(1, baseLine)];
    baseLineEnd = [staveLinesEnds(2, baseLine) staveLinesEnds(1, baseLine)];
    %Perform note processing
    %sounds
    soundsCount = 0;
    notesCount = 0;
    lastX = 0;
    for j=1:recognisedComponentCount
        class = recognisedComponents(j).class;
        absCenter = recognisedComponents(j).absCenter;
        if ~types(class).isNote
            continue;
        end
        if abs(absCenter(2) - lastX) > staveDist
            %New sound, assume duration
            soundsCount = soundsCount + 1;
            notesCount = 0;
            switch class
                case 2
                    duration = durations(1);
                case 3
                    duration = durations(2);
                case 4
                    duration = durations(3);
                case 5
                    duration = durations(4);
                case 6
                    duration = durations(5);
                otherwise
                    duration = durations(1);
            end
            sounds(soundsCount).duration = duration;
        end
        dist = linePointDistance(absCenter, baseLineStart, baseLineEnd);
        normShift = -round(dist / (staveDist/2));
        currentClearNote = baseNote + normShift;
        currentOctave = baseOctave;
        if currentClearNote > length(clearNotes)
            shiftOctave = floor(currentClearNote/length(clearNotes));
            currentOctave = currentOctave + shiftOctave;
            currentClearNote = currentClearNote - shiftOctave*length(clearNotes);
        end
        if currentClearNote < 1
            shiftOctave = ceil(currentClearNote/length(clearNotes)) - 1;
            currentOctave = currentOctave + shiftOctave;
            currentClearNote = currentClearNote - shiftOctave*length(clearNotes);            
        end
        notesCount = notesCount + 1;
        sounds(soundsCount).notes(notesCount).octave = currentOctave;
        sounds(soundsCount).notes(notesCount).clearNote = currentClearNote;
        sounds(soundsCount).notes(notesCount).line = normShift;
        if debug ~= 0
            components(recognisedIdx(j)).normShift = normShift;
            components(recognisedIdx(j)).octave = currentOctave;
            components(recognisedIdx(j)).clearNote = currentClearNote;
        end
    end
    %Display debug info
    if debug ~= 0
        fontSize = 8;
        figure; imshow([onlyComponents; staveImg; staveImgClosed]);
        hold on
        %Display stave lines
        for j=1:length(staveLinesStarts)
            if j == baseLine
                color = 'y';
            else
                color = 'w';
            end
            Xs = [staveLinesStarts(1, j) staveLinesEnds(1, j)];
            Ys = [staveLinesStarts(2, j) staveLinesEnds(2, j)];
            line(Xs, Ys, 'Color', color);
        end
        %Display components
        for j=1:componentsCount
            %Draw parent box
            if ~isempty(components(j).parentBox)
                parentBox = components(j).parentBox;
                rectangle('Position', parentBox, 'EdgeColor', 'b');
            end
            %Draw component
            box = components(j).box;
            class = components(j).class;
            correlation = components(j).correlation;
            if class == 1
                color = 'r';
                label = [num2str(j)];
            else
                color = 'g';
                label = {...
                    [num2str(j) ': ' types(class).Label];...
                    [num2str(correlation, 3)]...
                };
                if types(class).isNote
                    label{end+1} = [...
                        num2str(components(j).normShift) ' '...
                        num2str(components(j).octave) '-'...
                        num2str(components(j).clearNote)...
                    ];
                end
                scatter(components(j).absCenter(2), components(j).absCenter(1), 'filled');
            end
            rectangle('Position', box, 'EdgeColor', color);
            x = floor(max(box(1), 1)); xEnd = floor(min(x + box(3) - 1, sw));
            y = floor(max(box(2), 1)); yEnd = floor(min(y + box(4) - 1, sh));
            text(x, yEnd + length(label)*fontSize/2, label, 'Color', color, 'FontSize', fontSize);
        end
        hold off
    end
    staveSounds(staveCount).sounds = sounds;
end
save(['target/' filename '.sounds.mat'], 'staveSounds');