clear all; close all
addpath 'prepare';
addpath 'utils';
addpath 'clustering'; 
filename = 'test1.png';
debug = 1;
I = imread(['images/' filename]);
[imgs, meanStaveDist, ~, ~] = prepareDataset(I, 'Debug', debug);
%% Display dataset
disp('Output images');
a = ceil(sqrt(length(imgs)));
figure;
for i=1:length(imgs)
    subplot(a, a, i); imshow(imgs(i).img);
end
%% Count distances
sizes = zeros(length(imgs), 2);
for i=1:length(imgs)
    notes{i} = double(imgs(i).img);
    sizes(i, :) = imgs(i).size;
end
distances = countImgNormDistance(notes, 'Debug', debug);
%% Make cluster marks
clusters = linkedClustering(distances, 'Debug', debug);
for i=1:length(clusters)
    imgs(i).cluster = clusters(i);
end
%% Display clusters
if debug ~= 0
    clustersCount = max(clusters);
    clustersSize = accumarray(clusters, 1);
    maxSize = max(sizes, [], 1);
    out = zeros(maxSize(1) * clustersCount, maxSize(2) * max(clustersSize));
    xPerCluster = ones(clustersCount, 1);
    for i = 1:length(imgs)
        img = imgs(i).img;
        currentCluster = imgs(i).cluster;
        x = xPerCluster(currentCluster);
        y = (currentCluster - 1) * maxSize(1) + 1;
        out(y:(y+imgs(i).size(1)-1), x:(x+imgs(i).size(2))-1) = img;
        xPerCluster(currentCluster) = xPerCluster(currentCluster) + maxSize(2);
    end
    figure; imshow(out); title('Clustered');
end
%% Write output
save(['target/' filename '.clust.mat'], 'imgs', 'meanStaveDist');