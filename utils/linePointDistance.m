function [dist] = linePointDistance(point, lineStart, lineEnd)
%LINEPOINTDISTANCE Count distance between point and line. Can be used for
%3d and 2d
if length(point) == 2
    point = [point 1];
end
if length(lineStart) == 2
    lineStart = [lineStart 1];
end
if length(lineEnd) == 2
    lineEnd = [lineEnd 1];
end
% Count distance
a = lineStart - lineEnd;
b = point - lineEnd;
c = cross(a,b);
dist = norm(c) / norm(a);
if c(3) < 0
    dist = -dist;
end
end

