function [dist] = countImgDistance(imgs, varargin)
%COUNTDISTANCE Summary of this function goes here
%   Detailed explanation goes here
%% PARSE INPUT
p = inputParser;
addRequired(p, 'imgs');
addOptional(p, 'Debug', 0);
parse(p, imgs, varargin{:});
debug = p.Results.Debug;
%% Prepare data
n = length(imgs);
totalSteps = n*(n-1)/2;
dist = zeros(1, totalSteps);
ss = zeros(n, 1);
for i = 1:n
    ss(i) = sum(sum(imgs{i}.^2));
end
wtb = 0;
if debug ~= 0 
    wtb=waitbar(0,'Please wait...','CreateCancelBtn',...
        'setappdata(gcbf,''canceling'',1)');
    setappdata(wtb,'canceling',0)
    set(wtb,'Name','Distance counting...');
end
try
    k = 1;
    for i=1:n-1
        note1 = imgs{i};
        for j=i+1:n
            note2 = imgs{j};
            corr=xcorr2(note1, note2);    
            m=max(corr(:));
            dist(k)=ss(i)+ss(j)-2.*m;
            k = k + 1;
        end
        if debug ~= 0
            if getappdata(wtb, 'canceling')
                break
            end
            waitbar(k/totalSteps, wtb);
        end
    end
    if debug ~= 0
        delete(wtb);
    end
catch ME
    if debug ~= 0
        delete(wtb);
    end
    rethrow(ME);
end
end

