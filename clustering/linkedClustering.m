function [clusters] = linkedClustering(distances, varargin)
%LINKEDCLUSTERING Summary of this function goes here
%   Detailed explanation goes here
%% PARSE INPUT
p = inputParser;
addRequired(p, 'distances');
addOptional(p, 'Debug', 0);
parse(p, distances, varargin{:});
debug = p.Results.Debug;
%% Perform clustering
links = linkage(distances, 'single');
figure; dendrogram(links,100);
%Split
inc = inconsistent(links);
clusters = cluster(links, 'cutoff', max(inc(:, 4)) * 0.95);
end

