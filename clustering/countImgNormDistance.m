function [dist] = countImgNormDistance(imgs, varargin)
%COUNTDISTANCE Summary of this function goes here
%   Detailed explanation goes here
%% PARSE INPUT
p = inputParser;
addRequired(p, 'imgs');
addOptional(p, 'Debug', 0);
parse(p, imgs, varargin{:});
debug = p.Results.Debug;
%% Prepare data
n = length(imgs);
totalSteps = n*(n-1)/2;
dist = zeros(1, totalSteps);
ss = zeros(n, 1);
for i = 1:n
    imgSize = size(imgs{i});
    s = max(imgSize);
    pad = floor((s - imgSize)/2);
    imgs{i} = padarray(imgs{i}, pad);
end
wtb = 0;
if debug ~= 0 
    wtb=waitbar(0,'Please wait...','CreateCancelBtn',...
        'setappdata(gcbf,''canceling'',1)');
    setappdata(wtb,'canceling',0)
    set(wtb,'Name','Distance counting...');
end
try
    k = 1;
    for i=1:n-1
        note1 = imgs{i};
        size1 = size(note1);
        for j=i+1:n
            note2 = imgs{j};
            size2 = size(note2);
            %Make them same size (biggest)
            if size1(1) > size2(1) && size1(2) > size2(2)
                corr=normxcorr2(note1, imresize(note2, size1)); 
            else
                corr=normxcorr2(imresize(note1, size2), note2);   
            end   
            m=max(corr(:));
            dist(k)=1 - m;
            k = k + 1;
        end
        if debug ~= 0
            if getappdata(wtb, 'canceling')
                break
            end
            waitbar(k/totalSteps, wtb);
        end
    end
    if debug ~= 0
        delete(wtb);
    end
catch ME
    if debug ~= 0
        delete(wtb);
    end
    rethrow(ME);
end
end

